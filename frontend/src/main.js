import Vue from 'vue';
import ElementUI from 'element-ui';
import axios from 'axios';
import App from './App.vue';
import router from './router';
import store from './store';

import 'element-ui/lib/theme-chalk/index.css';
/* eslint no-param-reassign: "error" */
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use({
  install(vue) {
    vue.prototype.$api = axios.create({
      baseURL: 'http://localhost:4000/api/',
    });
  },
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
