﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Configuration;
using MongoDB.Bson.Serialization.Attributes;
using System.Globalization;

namespace PipelineRunner
{
    class Tasks
    {
        public ObjectId Id { get; set; }
        public string TaskId { get; set; }
        public string name { get; set; }
        public int av_time { get; set; }
        public string current_pipeline { get; set; }
    }

    class Pipelines
    {
        public ObjectId Id { get; set; }
        public string PipelineId { get; set; }
        public string name { get; set; }
        public string[] tasks { get; set; }

        public int run_time { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "mongodb://localhost";

            string id = args[0];//  "5ee259234d1e6c52acf208400";

            MongoClient client = new MongoClient(connectionString);
            var database = client.GetDatabase("cyntegrity");
            var pipeline = FindPipeline(database, id).GetAwaiter().GetResult();
            var run_time = RunTask(database, pipeline).GetAwaiter().GetResult();
            SaveRunTime(database, pipeline.PipelineId, run_time).GetAwaiter().GetResult();
        }

        private static async Task<Pipelines> FindPipeline(IMongoDatabase db, string id)
        {
            var collection = db.GetCollection<Pipelines>("pipelines");
            var pipeline = await collection.Find(new BsonDocument("PipelineId", id)).SingleAsync();
             
            return pipeline;

        }

        private static async Task<int> RunTask(IMongoDatabase db, Pipelines pipeline)
        {
            var collection = db.GetCollection<Tasks>("tasks");

            int pipeline_run_time = 0;

            if (pipeline.tasks.Length > 0)
            {
                var random = new Random();
                foreach (string id in pipeline.tasks)
                {
                    var task = await collection.Find(new BsonDocument("TaskId", id)).SingleAsync();
                    if (task != null)
                    {
                        var run_time = random.Next(100, 3000);
                        pipeline_run_time += run_time;
                        Thread.Sleep(run_time);
                        Console.Out.WriteLine(task.av_time);
                    }
                }
            }

            return pipeline_run_time;
        }

        private static async Task SaveRunTime(IMongoDatabase db, string id, int run_time)
        {
            var collection = db.GetCollection<Pipelines>("pipelines");
            await collection.UpdateOneAsync(
                new BsonDocument("PipelineId", id),
                new BsonDocument("$set", new BsonDocument("run_time", run_time))
            );
        }
   
    }
}