const express = require("express")
const mongoose = require("mongoose")
const child_process  = require('child_process');
const { v4: uuidv4 } = require('uuid');
const cors = require('cors')

const app = express()

const Schema = mongoose.Schema
const jsonParser = express.json()

const taskScheme = new Schema({
    TaskId: String,
    name: String,
    av_time: Number,
    current_pipeline: String
}, {versionKey: false});
const Task = mongoose.model("Task", taskScheme);

const pipelineScheme = new Schema({
    PipelineId: String,
    name: String,
    tasks: Array,
    run_time: Number
}, {versionKey: false});
const Pipeline = mongoose.model("Pipeline", pipelineScheme);

app.use(cors())

mongoose.connect("mongodb://localhost:27017/cyntegrity", { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: true }, function(err){
    if(err) return console.log(err);
    app.listen(3000, function(){
        console.log("Wait connect...");
    });
});

app.get('/api/tasks/', (req, res) => {
    Task.find({}, function(err, users){
        if(err) return console.log(err);
        res.send(users)
    });
})

app.post("/api/tasks", jsonParser, function (req, res) {
        
    if(!req.body) return res.sendStatus(400);
        
    const taskName = req.body.name;
    const taskAverage = parseInt(req.body.time);
    const task = new Task({
        name: taskName,
        av_time: taskAverage,
        TaskId: uuidv4(),
        current_pipeline: null
    });
        
    task.save(function(err){
        if(err) return console.log(err);
        res.send(task);
    });
});

app.put("/api/tasks", jsonParser, function (req, res) {
    if(!req.body) return res.sendStatus(400);

    const id = req.body.id;
    const current_pipeline = req.body.current_pipeline;
    const newTask = { current_pipeline };

    Task.findOneAndUpdate({TaskId: id}, newTask, {new: true}, function(err, task){
        if(err) return console.log(err); 
        res.send(task);
    });
})

app.get('/api/pipelines/', (req, res) => {
    Pipeline.find({}, function(err, pipelines){
        if(err) return console.log(err);
        res.send(pipelines)
    });
})

app.post("/api/pipelines", jsonParser, function (req, res) {
        
    if(!req.body) return res.sendStatus(400);
        
    const pipelineName = req.body.name;
    const pipelineTasks = req.body.tasks;
    const pipeline = new Pipeline({
        name: pipelineName,
        tasks: Array.from(pipelineTasks),
        PipelineId: uuidv4(),
        run_time: 0
    });
        
    pipeline.save(function(err){
        if(err) return console.log(err);
        res.send(pipeline);
    });
});

app.put("/api/pipelines", jsonParser, function(req, res){

    if(!req.body) return res.sendStatus(400);
    const id = req.body.id;
    const pipelineTasks = req.body.tasks;
    const newPipeline = { tasks: Array.from(pipelineTasks), run_time: 0 };

    Pipeline.findOneAndUpdate({PipelineId: id}, newPipeline, {new: true}, function(err, pipeline){
        if(err) return console.log(err); 
        res.send(pipeline);
    });
});

app.post("/api/pipelines/run", jsonParser, function (req, res) {
        
    if(!req.body) return res.sendStatus(400);
        
    const pipelineId = req.body.id;

    const proc = child_process.spawn('../runner/PipelineRunner/bin/Debug/PipelineRunner.exe',[pipelineId], {shell: false, detached : true})

    let run_time = 0;

    proc.on('error', function (error) {throw error;});
    proc.stdout.on('data', function (data) {
        console.log('stdout: ' + data);
        run_time+=parseInt(data);
    });
    proc.stderr.on('data', function (data) {
        console.log('stderr: ' + data);
        res.send({
            done: false,
            result: 0
        })
    });
    proc.on('close', function (code, signal) {
        console.log('Child process exited with code ' + code);
        res.send({
            done: true,
            result: run_time
        })
    });
    proc.on('exit', function (code, signal) {
        console.log('Child exited with code ' + code);
    });
});




app.listen(4000)